# CyberSource POS Test Tool iOS #

* v0.1.0

### What is this repository for? ###

This tool allows an iOS Device to connect to terminal to perform test transactions.
The app allows for selction of Currency, Terminal Type and CyberSource Environment.

### How do I get set up? ###

* Clone this repo
* Install XCode and CocoaPods
* Navigate to the cloned directory in a terminal and 'pod install'
* Open the project in XCode and select your device
* Deploy and Test!