//
//  Currency.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 16/01/2018.
//  Copyright © 2018 Jamie Haughton. All rights reserved.
//

import Foundation

class Currency {
    var pwValue : MPCurrency
    var symbol : String
    var isoStandard : String
    
    init(withIsoStandard : String, symbol : String, pwValue : MPCurrency) {
        self.pwValue = pwValue
        self.symbol = symbol
        self.isoStandard = withIsoStandard
    }
}
