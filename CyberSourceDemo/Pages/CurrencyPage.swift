//
//  CurrencyPage.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 16/01/2018.
//  Copyright © 2018 Jamie Haughton. All rights reserved.
//

import Foundation

class CurrencyPage : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var currencyPicker: UIPickerView!
    
    let GBP = Currency(withIsoStandard: "GBP", symbol: "£", pwValue: .GBP)
    
    let pickerDataSource = [
        Currency(withIsoStandard: "GBP", symbol: "£", pwValue: .GBP),
        Currency(withIsoStandard: "USD", symbol: "$", pwValue: .USD),
        Currency(withIsoStandard: "EUR", symbol: "€", pwValue: .EUR)];
    
    var parentPage : DevTestPageViewController!
    
    override func viewDidLoad() {
        parentPage = self.parent as! DevTestPageViewController
        currencyPicker.delegate = self
        currencyPicker.dataSource = self
        parentPage.transaction.currency = GBP
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row].isoStandard
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        parentPage.transaction.currency = pickerDataSource[row]
    }
    
}
