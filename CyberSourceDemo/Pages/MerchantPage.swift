//
//  CurrencyPage.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 16/01/2018.
//  Copyright © 2018 Jamie Haughton. All rights reserved.
//

import Foundation

class MerchantPage : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var merchantPicker: UIPickerView!
    
    let pickerDataSource = [MerchantDetails.ICL, .CAS];
    
    var parentPage : DevTestPageViewController!
    
    override func viewDidLoad() {
        parentPage = self.parent as! DevTestPageViewController
        merchantPicker.delegate = self
        merchantPicker.dataSource = self
        parentPage.transaction.merchant =
        .ICL
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row].asString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        parentPage.transaction.merchant = pickerDataSource[row]
    }
    
}

