//
//  SummaryPage.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 16/01/2018.
//  Copyright © 2018 Jamie Haughton. All rights reserved.
//

import Foundation
class SummaryPage : UIViewController {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var merchantLabel: UILabel!
    @IBOutlet weak var TerminalLabel: UILabel!
    
    var parentView : DevTestPageViewController!
    
    override func viewDidLoad() {
        parentView = self.parent as! DevTestPageViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let transaction = parentView.transaction
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = formatter.nilSymbol
        amountLabel.text = transaction.currency!.symbol + formatter.string(from: transaction.amount!)!
        merchantLabel.text = transaction.merchant?.asString
        TerminalLabel.text = transaction.terminal?.asString
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let transaction = parentView.transaction
        TerminalLabel.text = transaction.terminal?.asString
    }
    
    @IBAction func payButton(_ sender: UIButton) {
        let transaction = parentView.transaction
        let amount = transaction.amount!
        let currency = transaction.currency!
        let merchant = transaction.merchant!
        let terminal = transaction.terminal!
        
        let ui = MPUMposUi.initialize(with: MPProviderMode.TEST, merchantIdentifier: merchant.identifier, merchantSecret: merchant.secretKey)
        
        let accessoryParameters = terminal.accessoryParameters
        
        if (amount.compare(NSDecimalNumber.zero) == .orderedSame) {
            let alert = UIAlertController.init(title: "Error", message: "Zero Amount Transactions are not allowed.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        let transactionParameters = MPTransactionParameters.charge(withAmount: amount, currency: currency.pwValue) { (optional : MPTransactionParametersOptionals) in
            optional.subject = "POS Test Tool"
        }
        
        ui.configuration.terminalParameters = accessoryParameters
        ui.configuration.summaryFeatures = [.sendReceiptViaEmail, .refundTransaction]
        
        let viewController = ui.createTransactionViewController(with: transactionParameters, completed: { (viewController : UIViewController, result: MPUTransactionResult, transaction : MPTransaction?) in
            self.dismiss(animated: false, completion: nil)
            
            let alert = UIAlertController.init(title: "Transaction Result", message: "default", preferredStyle: UIAlertControllerStyle.alert)
            
            if (result == MPUTransactionResult.approved) {
                alert.message = "Payment was Approved!"
            } else {
                alert.message = "Payment was declined."
            }
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)
            
            self.present(alert, animated: true, completion: nil)
        })
        
        let modalNav = UINavigationController.init(rootViewController: viewController)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone) {
            modalNav.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        } else {
            modalNav.modalPresentationStyle = UIModalPresentationStyle.formSheet
        }
        self.present(modalNav, animated: true, completion: nil)
    }
}
