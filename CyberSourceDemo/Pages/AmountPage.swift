//
//  AmountPage.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 16/01/2018.
//  Copyright © 2018 Jamie Haughton. All rights reserved.
//

import Foundation
import TRCurrencyTextField

class AmountPage : UIViewController {
    @IBOutlet weak var amountField: TRCurrencyTextField!
    
    var parentView : DevTestPageViewController!
    
    override func viewWillAppear(_ animated: Bool) {
        parentView = self.parent as! DevTestPageViewController
        amountField.currencyCode = parentView.transaction.currency?.isoStandard
        amountField.setNeedsDisplay()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        parentView.transaction.amount = NSDecimalNumber(decimal : amountField.value.decimalValue)
    }
}
