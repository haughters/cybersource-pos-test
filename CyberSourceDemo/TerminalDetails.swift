//
//  TerminalDetails.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 15/11/2017.
//  Copyright © 2017 Jamie Haughton. All rights reserved.
//

enum TerminalDetails {
    case MIURA
    case VERIFONE
    
    var accessoryParameters : MPAccessoryParameters {
        switch self {
        case .MIURA:
            return miuraAccessoryParameters()
        case .VERIFONE:
            return verifoneAccessoryParameters()
        }
    }
    
    var asString : String {
        switch self {
        case .MIURA:
            return "Bluetooth Miura"
        case .VERIFONE:
            return "Verifone e355"
        }
    }
    
    fileprivate func miuraAccessoryParameters() -> MPAccessoryParameters {
        return MPAccessoryParameters.externalAccessoryParameters(with: .miuraMPI, protocol: "com.miura.shuttle", optionals: nil)
    }
    
    fileprivate func verifoneAccessoryParameters() -> MPAccessoryParameters {
        return MPAccessoryParameters.tcpAccessoryParameters(with: .verifoneVIPA, remote: "192.168.137.101", port: 16107, optionals: nil)
    }
    
}
