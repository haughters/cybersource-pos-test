//
//  MerchantDetails.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 15/11/2017.
//  Copyright © 2017 Jamie Haughton. All rights reserved.
//

import Foundation

enum MerchantDetails {
    case CAS
    case ICL
    
    var identifier : String {
        switch self {
        case .CAS:
            return "cfa36e55-a234-4acb-b3d8-2a8662f3556e"
        case .ICL:
            return "93ee4c6d-4a42-4948-b4dc-ecda4c07d910"
        }
    }
    
    var secretKey : String {
        switch self {
        case .CAS:
            return "b0GbS7ShDVUZbQPKRNkQx2Y4BMZddqEB"
        case .ICL:
            return "n6XRl9w66j7hxqZwUVcfsXp8aqUd330y"
        }
    }
    
    var asString : String {
        switch self {
        case .CAS:
            return "CAS"
        case .ICL:
            return "ICL"
        }
    }
    
}
