//
//  DevTestPageViewController.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 10/01/2018.
//  Copyright © 2018 Jamie Haughton. All rights reserved.
//

import UIKit

class DevTestPageViewController: UIPageViewController {
    
    var transaction = Transaction()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        dataSource = self
    
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
            }
    }

    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newColoredViewController(name: "Begin"),
                self.newColoredViewController(name: "Currency"),
                self.newColoredViewController(name: "Amount"),
                self.newColoredViewController(name: "Merchant"),
                self.newColoredViewController(name: "Terminal"),
                self.newColoredViewController(name: "Summary")]
    }()

    private func newColoredViewController(name: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(name)Page")
    }
    
    @IBAction func next(_ sender: Any) {
        self.setViewControllers(orderedViewControllers, direction: .forward, animated: true, completion: nil)
    }
    

}

// MARK: UIPageViewControllerDataSource

extension DevTestPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

struct Transaction {
    var amount : NSDecimalNumber?
    var currency : Currency?
    var terminal : TerminalDetails?
    var merchant : MerchantDetails?
    
    init(withAmount: NSDecimalNumber? = nil,
         withCurrency: Currency? = nil,
         withTerminal: TerminalDetails? = nil,
         withMerchant: MerchantDetails? = nil) {
        self.amount = withAmount
        self.currency = withCurrency
        self.merchant = withMerchant
        self.terminal = withTerminal
    }
}


