//
//  ViewController.swift
//  CyberSourceDemo
//
//  Created by Jamie Haughton on 29/09/2017.
//  Copyright © 2017 Jamie Haughton. All rights reserved.
//

import UIKit
import TRCurrencyTextField

class ViewController: UIViewController {

    @IBOutlet weak var amountTextField: TRCurrencyTextField!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var terminalSelector: UISegmentedControl!
    @IBOutlet weak var currencySelector: UISegmentedControl!
    @IBOutlet weak var environmentSelector: UISegmentedControl!
    
    let payLabel = "Pay"
    var total = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amountTextField.currencyCode = "GBP"
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateCurrency(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 0) {
            amountTextField.currencyCode = "GBP"
        } else {
            amountTextField.currencyCode = "EUR"
        }
                    amountTextField.setNeedsDisplay()
    }
    
    fileprivate func getMerchantEnvironment() -> MerchantDetails {
        if (environmentSelector.selectedSegmentIndex == 0) {
            return .CAS
        } else {
            return .ICL
        }
    }
    
    fileprivate func getTerminalDetails() -> TerminalDetails {
        if (terminalSelector.selectedSegmentIndex == 0) {
            return .MIURA
        } else {
            return .VERIFONE
        }
    }
    
    fileprivate func getCurrencyDetails() -> MPCurrency {
        if (currencySelector.selectedSegmentIndex == 0) {
            return .GBP
        } else {
            return .EUR
        }
    }
    
    @IBAction func pay(_ sender: UIButton) {
        let merchantDetails = getMerchantEnvironment()
        let terminalDetails = getTerminalDetails()
        
        let ui = MPUMposUi.initialize(with: MPProviderMode.TEST, merchantIdentifier: merchantDetails.identifier, merchantSecret: merchantDetails.secretKey)
        
        let accessoryParameters = terminalDetails.accessoryParameters
        
        let total = NSDecimalNumber(decimal : amountTextField.value.decimalValue)
        
        if (total.compare(NSDecimalNumber.zero) == .orderedSame) {
            let alert = UIAlertController.init(title: "Error", message: "Zero Amount Transactions are not allowed.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        let transactionParameters = MPTransactionParameters.charge(withAmount: total, currency: getCurrencyDetails()) { (optional : MPTransactionParametersOptionals) in
            optional.subject = "POS Test Tool"
        }
        
        ui.configuration.terminalParameters = accessoryParameters
        ui.configuration.summaryFeatures = [.sendReceiptViaEmail, .refundTransaction]
        
        let viewController = ui.createTransactionViewController(with: transactionParameters, completed: { (viewController : UIViewController, result: MPUTransactionResult, transaction : MPTransaction?) in
            self.dismiss(animated: false, completion: nil)

            let alert = UIAlertController.init(title: "Transaction Result", message: "default", preferredStyle: UIAlertControllerStyle.alert)

            if (result == MPUTransactionResult.approved) {
                alert.message = "Payment was Approved!"
            } else {
                alert.message = "Payment was declined."
            }

            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)

            self.present(alert, animated: true, completion: nil)
        })
        
        let modalNav = UINavigationController.init(rootViewController: viewController)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone) {
            modalNav.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        } else {
            modalNav.modalPresentationStyle = UIModalPresentationStyle.formSheet
        }
        self.present(modalNav, animated: true, completion: nil)
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

