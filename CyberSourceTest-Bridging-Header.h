@import Foundation;
@import SystemConfiguration;
@import MobileCoreServices;

#import <mpos.core/mpos.h>
#import <mpos-ui/mpos-ui.h>
#import <CommonCrypto/CommonCrypto.h>
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

#import <TRCurrencyTextField/TRCurrencyTextField.h>
